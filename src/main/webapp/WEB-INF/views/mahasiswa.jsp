<%
	request.setAttribute("contextName", request.getServletContext().getContextPath());
%>
<html>
<head>
<title>Mahasiswa</title>
<link rel="stylesheet" href="${contextName}/assets/bootstrap/css/bootstrap.min.css">
<script src="${contextName}/assets/jquery/jquery-3.3.1.min.js"></script>
<script src="${contextName}/assets/bootstrap/js/bootstrap.min.js"></script>
<style>
	body{
		padding-top:10%;
		background: url(${contextName}/assets/img/coll.jpg);
		background-size: cover;
		background-attachment:fixed;
		background-position: center;
		background-repeat: no-repeat;
	}
	
</style>
</head>
<body class=" panel-primary">
	<form method="POST" action="mahasiswa">
	<div class="wrapper">
		<div class="col-md-5 panel-primary" style="margin:auto;color:white;padding:19px;padding-bottom:100px;background:rgba(0,0,0,0.4);">
			<div class="form-group">
			    <label for="nim">NIM:</label>
			    <input type="text" class="form-control" id="nim">
			</div>
			<div class="form-group">
			    <label for="nama">Nama:</label>
			    <input type="text" class="form-control" id="nama">
			</div>
			  <div class="form-group">
			    <label for="alamat">Alamat:</label>
			    <textarea class="form-control" id="alamat"></textarea>
			  </div>
			  <div class="form-group">
			    <label for="tgl">Tanggal Lahir:</label>
			    <input type="date" class="form-control" id="tgl">
			  </div>
			  <button style="float:right;width:100%" type="submit" class="btn btn-warning">Submit</button>
		</div>
	</div>
	
	<!--
		<table>
			<tr>
				<td>NIM</td>
				<td><div class="form-group"><input class="form-control" type="text" value="" id="nim"></div></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td><div class="form-group"><input class="form-control" type="text" value="" id="nama"></div></td>
			</tr>
			<tr>
				<td>Tanggal Lahir</td>
				<td><div class="form-group"><input class="form-control" type="text" value="" id="tanggalLahir"></div></td>
			</tr>
		</table>
		-->
	</form>
</body>
</html>