package com.spring.marcoma.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spring.marcoma.model.Employee;

/**
 * @author Awiyanto Ajisasongko
 */
public interface EmployeeDao extends JpaRepository<Employee, Long> {

	@Query("SELECT m FROM Employee m WHERE m.isDelete=FALSE or m.isDelete IS NULL")
	public List<Employee> ambilDataYangAda();
	
	@Query("SELECT m FROM Employee m where id=:id")
	public Employee checkEmployeeId(@Param("id") Long id);
	
	@Query("SELECT m FROM Employee m WHERE m.firstName LIKE %:word% OR m.lastName LIKE %:word%")
	public List<Employee> search(@Param("word") String word);
	
	@Query(value="SELECT COUNT(*) FROM m_employee WHERE created_date = (SELECT created_date FROM M_EMPLOYEE ORDER BY created_date DESC LIMIT 1)", nativeQuery=true)
	public Long jumlahDataHaiIni();
	
}
