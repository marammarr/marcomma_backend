/**
 * 
 */
package com.spring.marcoma.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.marcoma.dao.EmployeeDao;
import com.spring.marcoma.model.Employee;

/**
 * @author Awiyanto Ajisasongko
 *
 */
@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
	@Autowired
	private EmployeeDao employeeDao;
	
	private Log log = LogFactory.getLog(getClass());
	
	//GET ONE
	@CrossOrigin
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public ResponseEntity<Employee> get(@PathVariable Long id) {
		try {
			Employee employee = this.employeeDao.findOne(id);
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);			
		}
	}
	
	//DELETE
	@CrossOrigin
	@RequestMapping(value="/", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody Employee employee) {
		try {
			Employee e = this.employeeDao.checkEmployeeId(employee.getId());
			if(e!=null) {
				e.setIsDelete(true);
				this.employeeDao.save(e);
				return new ResponseEntity<>("1",HttpStatus.OK);
			}else {
				return new ResponseEntity<>("0",HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);			
		}
	}

	//GET ALL
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ResponseEntity<List<Employee>> getAll() {
		try {
			List<Employee> employee = this.employeeDao.findAll();
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//INSERT
	@CrossOrigin
	@RequestMapping(value="/", method=RequestMethod.POST)
	public ResponseEntity<?> insert(@RequestBody Employee employee) {
		try {
			String prefix = "";
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			Long t = this.employeeDao.jumlahDataHaiIni()+1;
			System.out.println("TESSS "+t);
			prefix = (year%100)+"."+(month<10?"0"+month:month)+"."+(day<10?"0"+day:day)+".";
			String generatedId = t<10?prefix+"0"+t : prefix+t;
			
			employee.setCode(generatedId);
			this.employeeDao.save(employee);
			return new ResponseEntity<>("1", HttpStatus.CREATED);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);			
		}
	}	

	//UPDATE
	@CrossOrigin
	@RequestMapping(value="/", method=RequestMethod.PUT)
	public ResponseEntity<Employee> update(@RequestBody Employee employee) {
		try {
			this.employeeDao.save(employee);
			return new ResponseEntity<>(employee, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);			
		}
	}
}
