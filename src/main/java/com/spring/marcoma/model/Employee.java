/**
 * 
 */
package com.spring.marcoma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Awiyanto Ajisasongko
 *
 */
@Entity
@Table(name="M_EMPLOYEE", uniqueConstraints=@UniqueConstraint(columnNames= {"EMPLOYEE_NUMBER"}, name="UK_EMPLOYEE_NUM"))
public class Employee {
	@Id
	@GeneratedValue
	@Column(name="ID", nullable=false, length=11)
	private Long id;
	
	@GenericGenerator(name="seq_empl_code", strategy="com.spring.marcoma.function.EmployeeIdGenerator")
	@GeneratedValue(generator="seq_empl_code")
	@Column(name="EMPLOYEE_NUMBER", length=50, nullable=false)
	private String code;
	
	@Column(name="FIRST_NAME", length=50, nullable=false)
	private String firstName;

	@Column(name="LAST_NAME", length=50, nullable=true)
	private String lastName;
	
	@Column(name="M_COMPANY_ID", length=11, nullable=true)
	private Long mCompanyId;
	
	@Column(name="EMAIL", length=150, nullable=true)
	private String email;
	
	@Column(name="IS_DELETE", nullable=false, columnDefinition="boolean default false" )
	private Boolean isDelete;
	
	@Column(name="CREATED_BY", length=50, nullable=false)
	private String createdBy;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name="CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@Column(name="UPDATED_BY", length=50, nullable=true)
	private String updatedBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name="UPDATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getmCompanyId() {
		return mCompanyId;
	}

	public void setmCompanyId(Long mCompanyId) {
		this.mCompanyId = mCompanyId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
}
